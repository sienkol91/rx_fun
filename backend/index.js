import express from 'express'
import io from 'socket.io'
import Rx from 'rxjs/Rx'
import handler from './socket/handler'

const mySocket = io.listen(3000)

Rx.Observable
    .fromEvent(mySocket.sockets, 'connection')
    .subscribe(handler)
