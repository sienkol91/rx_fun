import Rx from 'rxjs/Rx'

export default (socket) => {
    console.log('in_handler_file')

    Rx.Observable
    .fromEvent(socket, 'send')
    .subscribe(e => {
        console.log(e)
    })
}
