const webpack = require('webpack');

const config = {
    context: __dirname + '/src', // `__dirname` is root of project and `src` is source
    entry: {
        app: './app.js',
    },
    output: {
        path: __dirname + '/dist', // `dist` is the destination
        filename: 'bundle.js',
    },
    devServer: {
        contentBase: __dirname + '/src', // `__dirname` is root of the project
    },
    module: {
        rules: [
            {
                test: /\.js$/, // Check for all js files
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: { presets: ['es2015'] }
                }]
            }
        ]
    },

    devtool: "eval-source-map" // Default development sourcemap
};

module.exports = config;