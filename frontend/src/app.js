import Rx from 'rxjs/Rx'
import * as io from 'socket.io-client';

const socket = io.connect('http://localhost:3000')

const testButton = document.getElementById('my-test-button');
const testInput = document.getElementById('my-test-input');
const testContainer = document.getElementById('my-test-container');

const writeInputToContainer = (input, container) => {
    container.innerText = input
}

let somePromise = new Promise((res, rej) => {
    setTimeout(() => {
        // console.log('resolved')
        res('resolved to subscriber')
    }, 500)
})

// somePromise.then(() => console.log('bla'))

const myPromise = Rx.Observable
    .fromPromise(somePromise)
    .subscribe(e => console.log(e))

const inputStream = Rx.Observable
    .fromEvent(testInput, 'input')
    // .debounce(200)
    .map(e => e.target.value)
    .map(e => Number(e))
    .map(e => {
        if (isNaN(e)) return 0
        return e
    })
    .map(e => e*2)
    .map(e => `...${e}...`)
    .subscribe(e => writeInputToContainer(e, testContainer))

Rx.Observable
    .fromEvent(testInput, 'input')
    .debounce(() => Rx.Observable.timer(1000))
    .map(e => e.target.value)
    // .filter(value => value.length > 5)
    .subscribe(value => {
        socket.emit('send', { msg: value })
        console.log(value)
    })

Rx.Observable
    .fromEvent(socket, 'message')
    .subscribe(msg => console.log(msg))

const windowClickStream = Rx.Observable
    .fromEvent(window, 'click')
    .map(e => {
        console.log('e')
        return e
    })
    // .subscribe(e => console.log(e))



// const buttonClickStream = Rx.Observable
//     .fromEvent(testButton, 'click')
//     .map(e => 'clickInBtn')
//     .withLatestFrom(inputStream)
//     .distinct()
//     .subscribe(e => console.log(e))
